import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import DataStore from './../stores/DataStore.js';

export default class Landing extends Component {
  render() {
    let page = DataStore.getPageBySlug('introduccion');

    return (
      <div className='homeWrapper'>
        <div className='introContent'>
          <div className='introText projectQuote' dangerouslySetInnerHTML={{__html: page.content.rendered}} />
          <Link to='/mapa'>Inicio</Link>
        </div>
      </div>
    );
  }
}
