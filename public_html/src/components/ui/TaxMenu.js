import React from 'react';
import DataStore from '../../stores/DataStore.js';

export default class TaxMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false
    };
    this.violenceRefs = [];
    this.techRefs = [];
  }

  handleTaxClick = e => {
    let ele = e.target;
    let id = +ele.dataset.taxid.replace('tax', '');
    let type = ele.dataset.type;
    DataStore.activateFromArray('violencia', [id]);
    ele.classList.add('active');
    //this.props.updateMarkers();
  };

  handleTechClick = e => {
    let ele = e.target;
    let rels = DataStore.getTaxonomiesRelationship()[ele.dataset.taxid];

    DataStore.activateFromArray('violencia', rels);

    this.techRefs.forEach(refKey => {
      this.refs[refKey].classList.remove('active');
    });

    ele.classList.toggle('active');
    this.props.updateMarkers();
  };

  createTaxMenuElements(type, taxonomies) {
    return taxonomies.map(tax => {
      let span = (
        <span
          key={tax.slug}
          ref={`tax${tax.id}`}
          className={tax.active ? 'tax active' : 'tax'}
          data-taxid={`tax${tax.id}`}
          data-type={type}
          onClick={
            type === 'violencia' ? this.handleTaxClick : this.handleTechClick
          }
          style={{ borderColor: tax.color }}
        >
          {tax.name}
        </span>
      );

      return span;
    });
  }

  getTaxMenu() {
    if (!this.state.loaded) {
      return null;
    }
    let taxonomies = DataStore.getTaxonomies();
    let violence = this.createTaxMenuElements(
      'violencia',
      taxonomies.violencia
    );
    let techniques = this.createTaxMenuElements(
      'tecnicas',
      taxonomies.tecnicas
    );

    return (
      <div id='taxonomiesMenu' className={this.props.grid}>
        <div className='typesViolence' ref='violenceContainer'>
          {violence}
        </div>
        <div className='typesTechnique' ref='techniquesContainer'>
          {techniques}
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      loaded: true
    });

    let taxonomies = DataStore.getTaxonomies();

    taxonomies.violencia.forEach(tax => {
      this.violenceRefs.push(`tax${tax.id}`);
    });

    taxonomies.tecnicas.forEach(tax => {
      this.techRefs.push(`tax${tax.id}`);
    });
  }

  render() {
    let menu = this.getTaxMenu();

    return menu;
  }
}
