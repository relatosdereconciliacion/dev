import React, { Component } from 'react';
import Drawing from './Drawing';

export default class Top extends Component {
  render() {
    return (
      <section className='drawingSection'>
        <Drawing />
      </section>
    );
  }
}
