import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { random, fitElement, getPercent, sizeFromPercentage } from '../utils/helpers';
export default class Marker extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    let imgW = 531;
    let imgH = 800;
    let treeW = imgW / 3;
    let treeH = imgH / 3;
    this.w = random(30, 80);
    let per = getPercent(this.w, treeW);
    let _w = sizeFromPercentage(per, treeW);
    let _h = sizeFromPercentage(per, treeH);
    this.dims = fitElement(treeW, treeH, _w + 100, _h + 100);
  }

  render() {
    let x = this.props.x;
    let cl = 'marker';

    if (x < 300) {
      cl += ' pushRight';
    } else if (x > window.innerWidth - 300) {
      cl += ' pushLeft';
    }

    return (
      <Link
        className={cl}
        ref={this.props.slug}
        data-slug={this.props.slug}
        to={this.props.path}
        style={{
          left: `${this.props.x}px`,
          top: `${this.props.y}px`,
          height: `${window.innerHeight - this.props.y}px`,
          width: `${this.w}px`
        }}>
        <span className='markerTitle'>{this.props.title}</span>
        <span className='markerCential'
          style={{
            width: `${this.w - 2}px`
          }}
        ></span>
        <span className='markerFrontal'
          style={{
            width: `${this.dims.w}px`,
            height: `${this.dims.w}px`,
            backgroundSize: `${this.dims.w * 3}px, ${this.dims.h * 3}px`
          }}
        ></span>
      </Link>
    );
  }
}
