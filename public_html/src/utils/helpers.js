let debounceTimer;

export function random(min, max, isFloat) {
  var random = Math.floor(Math.random() * (max - min)) + min;

  if (isFloat) {
    random = Math.random() * (max - min) + min;
  }

  return random;
};

export function sizeFromPercentage(percent, totalSize) {
  return percent / 100 * totalSize;
}

export function getPercent(section, total) {
  return (section / total) * 100;
}

export function debounce(hold) {
  hold = hold || 250;
  return new Promise((resolve, reject) => {
    // trick for runing code only when resize ends
    // https://css-tricks.com/snippets/jquery/done-resizing-event/
    clearTimeout(debounceTimer);

    debounceTimer = setTimeout(() => {
      resolve();
    }, hold);
  });
}

export function rgbToHex(r, g, b) {
  if (!r) {return;}
  var g = g || r;
  var b = b || r;

  var rgb = b | (g << 8) | (r << 16);
  return '#' + (0x1000000 + rgb).toString(16).slice(3);
}

// https://stackoverflow.com/a/6565988/3661186
export function fitElement(sourceW, sourceH, parentW, parentH) {
  const sourceRatio = sourceW / sourceH;
  const parentRatio = parentW / parentH;
  let w;
  let h;

  if (parentRatio > sourceRatio) {
    w = sourceW * parentH / sourceH;
    h = parentH;
  } else {
    w = parentW;
    h = sourceH * parentW / sourceW;
  }

  return {w: w, h: h};
}

// http://www.jacklmoore.com/notes/mouse-position/
export function getXY(e) {
  let rect = e.target.getBoundingClientRect();

  return {
    x: e.clientX - rect.left,
    y: e.clientY - rect.top
  };
}
